import { test, expect } from '@playwright/test';
import { LoginPage } from '../pages/login';
import { ProductPage } from '../pages/productPage'

test.describe('Inventory sort test', () => {
  let loginPage;
  let productPage;
  let page;
  let productNames = [];

  test.beforeAll('Set up all pages required', async({ browser }) => {
    page = await browser.newPage();
    loginPage = new LoginPage(page)
    productPage = new ProductPage(page)
  })
  
  test.beforeEach('Login test', async () => {
    await loginPage.navigateToURL('/')
    await loginPage.login('standard_user', 'secret_sauce')
    expect(await page.title()).toBe('Swag Labs')
    const unsortedProductName = await productPage.getProductNames();
    for(let product of unsortedProductName) {
      productNames.push(await product.innerText())
    }
  })

  test('Validate that the items are sorted in ascending order (A-Z)', async() => {
    const sortedProductName = [...productNames].sort()
    expect(...productNames).toEqual(...sortedProductName)
  })

  test('Validate that the items are sorted in descending order(Z-A)', async() => {
    await productPage.reverseOrder()
    const reversedProductNames = productNames.reverse();
    const unsortedProductName = await productPage.getProductNames();
    for(let i = 0; i < unsortedProductName.length; i++) {
      expect(await unsortedProductName[i].innerText()).toEqual(reversedProductNames[i])
    }
  })
})
