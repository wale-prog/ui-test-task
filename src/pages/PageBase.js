exports.PageBase = class PageBase {

  constructor(page) {
    this.page = page
  }

  async enterText(element, text) {
    await element.fill(text)
  }

  async click(element) {
    await element.click()
  }
}