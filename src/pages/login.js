import { PageBase } from "./PageBase"

exports.LoginPage = class LoginPage {

  constructor(page) {
    this.page = page
    this.pageBase = new PageBase(page)
    this.username_field = page.locator('[data-test="username"]')
    this.password_field = page.locator('[data-test="password"]')
    this.login_Button = page.locator('[data-test="login-button"]')
  }

  async navigateToURL(url) {
    await this.page.goto(url)
  }

  async login(username, password) {
    await this.pageBase.enterText(this.username_field, username)
    await this.pageBase.enterText(this.password_field, password)
    await this.pageBase.click(this.login_Button)
  }

}