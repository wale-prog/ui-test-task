import { PageBase } from "./PageBase"

exports.ProductPage = class ProductPage {

  constructor(page) {
    this.page = page
    this.PageBase = new PageBase(page)
    this.reverseProduct = page.locator('[data-test="product-sort-container"]')
  }

  async getProductNames() {
    let productName = await this.page.$$('.inventory_item_name')
    return await productName
  }
  async reverseOrder() {
    await this.reverseProduct.selectOption('za')
  }
}